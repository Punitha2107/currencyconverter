﻿using CurrencyConverter.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter.Helper
{
   
    public class WebUtility<T>
    {
       
        public async Task<T> Get(string url, string body)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = "GET";
          
            try
            {
                var response = await request.GetResponseAsync();
                using (var responseStream = response.GetResponseStream())
                {
                    var reader = new StreamReader(
                        responseStream ?? throw new InvalidOperationException(),
                        System.Text.Encoding.UTF8
                    );
                    var jsonString = reader.ReadToEnd();

                    var results = JsonConvert.DeserializeObject<T>(jsonString);
                    return results;
                }
            }
            catch (WebException ex)
            {
                var errorResponse = ex.Response;
                using (var responseStream = errorResponse.GetResponseStream())
                {
                    var reader = new StreamReader(
                        responseStream ?? throw new InvalidOperationException(),
                        System.Text.Encoding.GetEncoding("utf-8")
                    );
                    string status = reader.ReadToEnd();
                    throw new JsonWebApiException(status, ex.Message, ex);
                }

            }
        }
    }


    public class JsonWebApiException : Exception
    {
        public string ResponseBody { get; set; }

        public JsonWebApiException(string responseBody)
        {
            ResponseBody = responseBody;
        }

        public JsonWebApiException(string responseBody, string message) : base(message)
        {
            ResponseBody = responseBody;
        }

        public JsonWebApiException(string responseBody, string message, Exception exception) : base(message, exception)
        {
            ResponseBody = responseBody;
        }
    }

    public static class RepositoryDbMock
    {
        public static CurrencyDBContext GetDbContext(string dbName)
        {
            var options = new DbContextOptionsBuilder<CurrencyDBContext>()
                .UseInMemoryDatabase(databaseName: dbName)
                .Options;
            var dbContext = new CurrencyDBContext(options);
            dbContext.Seed();
            return dbContext;
        }
    }

    public static class DbContextExtensions
    {
        /// <summary>
        /// This function will seed  the data for each object on every instance creation of the parent record.
        /// ALl this data is stored is in built memory of .net core
        /// </summary>
        /// <param name="dbContext"> A complex object which performs the object relation mapping</param>
        public static void Seed(this CurrencyDBContext dbContext)
        {
        }
    }
}
