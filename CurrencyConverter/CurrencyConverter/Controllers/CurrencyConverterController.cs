﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CurrencyConverter.Helper;
using CurrencyConverter.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CurrencyConverter.Models;

namespace CurrencyConverter.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class CurrencyConverterController : ControllerBase
    {
        protected  CurrencyDBContext RepositoryContext { get; private set; }

        public CurrencyConverterController()
        {
            this.RepositoryContext = RepositoryDbMock.GetDbContext("TestDB");
        }
        public class CurrencyInputParameters
        {
            /// <summary>
            /// The value of currency to be converted
            /// </summary>
            [DefaultValue("7900.9685")]
            [Required]         
            public double Amount { get; set; }
            
             /// <summary>
             /// From currency which the value has to be converted
             /// </summary>
            [Required]
            [DefaultValue("INR")]
            public string FromCurrency { get; set; }
            
            /// <summary>
            /// To Currency which the value has to be converted
            /// </summary>
            [Required]
            [DefaultValue("EUR")]
            public string ToCurrency { get; set; }
        }

        private readonly string appid = "28527a8eb10f49febd8a50c01051f292";
        public class CurrencyOutput
        { 
            /// <summary>
            /// The Amount of currency to be converted
            /// </summary>
            [Required]
            public double  Amount { get; set; }
            
            /// <summary>
            /// Name of the currency to be retrieved.Note: Exact Currency code should be given Please refer Get cureencies API to get the Code
            /// </summary>
            [Required]
            public string CurrencyName { get; set; }

           /// <summary>
           /// Addtional Or summary information of the conversion
           /// </summary>
            public string AddtionalInfo { get; set; }
        }

        /// <summary>
        /// These are the currency obtains from openexchangerates.com
        /// </summary>
        public class CurrencyRates
        {
           
            public string disclaimer { get; set; }
            /// <summary>
            /// Liscense of openexchangerates.com
            /// </summary>
            public string license { get; set; }
            /// <summary>
            /// The timestamp when the currency rate is extracted 
            /// </summary>
            public string timestamp { get; set; }
            /// <summary>
            /// The Base value of the conversion is always dollors
            /// </summary>
            public string @base { get; set; }

            /// <summary>
            /// The latest currency rates
            /// </summary>
            public Dictionary<string,double> rates { get; set; }
        }
        /// <summary>
        /// Converts between different currencies
        /// </summary>
        /// <param name="currencyInputParameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/Convert")]
        public CurrencyOutput ConvertCurrency([FromQuery] CurrencyInputParameters currencyInputParameters)
        {
            CurrencyOutput result = new CurrencyOutput();

            try
            {
                WebUtility<CurrencyRates> webUtility = new Helper.WebUtility<CurrencyRates>();
                string url = "https://openexchangerates.org/api/latest.json?app_id=" + appid;
                var currencyRates = webUtility.Get(url, "").Result;

                var from = Convert.ToDouble(currencyRates.rates[currencyInputParameters.FromCurrency.ToUpper()]);
                var to = Convert.ToDouble(currencyRates.rates[currencyInputParameters.ToCurrency.ToUpper()]);

                var dollorfrom = currencyInputParameters.Amount / from;
                var dollorto = dollorfrom * to;

                result.Amount = dollorto;
                result.CurrencyName = currencyInputParameters.ToCurrency;
                result.AddtionalInfo = string.Format("{0} {1} are converted to {2}", currencyInputParameters.Amount.ToString("###.####"), currencyInputParameters.FromCurrency, currencyInputParameters.ToCurrency);
                LastTransactions lt = new LastTransactions();
                lt.Amount = result.Amount;
                lt.FromCurrency = currencyInputParameters.FromCurrency;
                lt.Id = System.Guid.NewGuid().ToString();
                lt.TransactionTime = DateTime.Now;
                lt.ToCurrency = result.CurrencyName;
                this.RepositoryContext.LastTransaction.Add(lt);
                this.RepositoryContext.SaveChanges();
            }
            catch(Exception ex)
            {
                throw  ex;

            }
            return result;
        }

       /// <summary>
       /// Retreives the list of currencies available for conversion.
       /// Note :The conversion base is dollor.
       /// </summary>
       /// <returns></returns>
        [HttpGet]
        [Route("/GetCurrencies")]
        public Dictionary<string, string> GetCurrencies()
        {
            WebUtility<Dictionary<string, string>> webUtility = new Helper.WebUtility<Dictionary<string, string>>();
          

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("app_id", appid );

            string url = "https://openexchangerates.org/api/currencies.json";

            var currencies = webUtility.Get(url, "").Result ;

            return currencies;
            
        }

        [HttpGet]
        [Route("/GetLastTransactions")]
        public List<LastTransactions> GetLastTransactions(int num = 10)
        {
           

            return  this.RepositoryContext.LastTransaction.OrderByDescending(x => x.TransactionTime).Take(num).ToList();

        }

    }
}
