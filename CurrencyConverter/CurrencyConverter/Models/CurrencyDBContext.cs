﻿using Microsoft.EntityFrameworkCore;


namespace CurrencyConverter.Models
{
    public class CurrencyDBContext : DbContext
    {
        public CurrencyDBContext(DbContextOptions options)
            :base(options)
        {

        }
        public DbSet<LastTransactions> LastTransaction { get; set; }
    }
}
