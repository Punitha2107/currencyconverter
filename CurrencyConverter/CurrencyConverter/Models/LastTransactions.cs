﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Models
{
    public class LastTransactions
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string  Id { get; set; }

        [Required]
        public double Amount { get; set; }

        /// <summary>
        /// Name of the currency to be retrieved.Note: Exact Currency code should be given Please refer Get cureencies API to get the Code
        /// </summary>
        [Required]
        public string FromCurrency { get; set; }

        /// <summary>
        /// Addtional Or summary information of the conversion
        /// </summary>
        public string ToCurrency { get; set; }

        /// <summary>
        /// Time which the conversion take place
        /// </summary>
        public DateTime TransactionTime { get; set; }
    }
}
