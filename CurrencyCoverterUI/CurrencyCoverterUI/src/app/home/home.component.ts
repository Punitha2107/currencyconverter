import { Component, OnInit } from '@angular/core';

import { WebservicesService } from 'src/app/webservices.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { CurrencyOutput, LastTransaction } from '../shared/CurrencyOutput.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  
  public amount:number = 0;
  public ctamount:number = 0;
  public fromcurrency:string = "";
  public tocurrency:string = "";
  public transactions:LastTransaction[];
  constructor(private httpClient:HttpClient, private postService:WebservicesService) { }
  
  baseUrl = "http://localhost:8080/";
  public localData: CurrencyOutput;
  ngOnInit() {
  }

  onSubmit() {   
   let url = this.baseUrl + "Convert?Amount="+ this.amount+"&FromCurrency=" + this.fromcurrency +"&ToCurrency=" + this.tocurrency;
  this.httpClient.get(url).subscribe((res : CurrencyOutput)=> 
  {         
    this.localData = res;
    this.ctamount = res.amount;
    console.log(this.localData.currencyname);
  },(error:any)=>{  
    console.log(error.message)
  });
    
  }


  getTransactions()
  {
    let url =this.baseUrl + "getlasttransactions"
    this.httpClient.get(url).subscribe((res : LastTransaction[])=> 
    {         
      this.transactions = res;     
      console.log(this.transactions.length.toString());
    },(error:any)=>{  
      console.log(error.message)
    });

  }
}

 


