import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WebservicesService {

  constructor(private httpClient: HttpClient) { }

  getPosts () {
    let url = environment.apiUrl 
    return this.httpClient.get<Array<any>>(url);
  }

  createPost (body) {
    let url = environment.apiUrl + 'posts';
    return this.httpClient.post(url, body)
  }
}
