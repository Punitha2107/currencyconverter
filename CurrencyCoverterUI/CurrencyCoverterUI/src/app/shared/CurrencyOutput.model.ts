export class CurrencyOutput{
    id:string;
    currencyname:string;
    amount:number;
    addtionalinfo:string;
 }

 export class LastTransaction{
    id:string;
    fromCurrency:string;
    amount:number;
    toCurrency:string;
    transactionTime:Date;
 }