using CurrencyConverter.Controllers;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace CurrencyConverter.Test
{
    public class CurrencyConversionUnitTest
    {
        public string[] currencies = new string[4] { "INR", "EUR", "AED" ,"USD"};

        [Test]
        public async Task TestConvertByDate()
        {

            CurrencyConverterController.CurrencyInputParameters currencyInputParameters = new CurrencyConverterController.CurrencyInputParameters();
            Random rnd = new Random();           
            int nums = rnd.Next(0, 100000);
            currencyInputParameters.Amount = nums;
            int idx = new Random().Next(0,3);
            String from = (currencies[idx]);
            currencyInputParameters.FromCurrency = from;
            int idx1 = new Random().Next(0, 3);
            String tocur = (currencies[idx1]);
            currencyInputParameters.ToCurrency = tocur;
            CurrencyConverterController.CurrencyOutput value  = new CurrencyConverterController.CurrencyOutput();            
            var controller = new CurrencyConverterController();
            var response =  controller.ConvertCurrency(currencyInputParameters);
            value = response;
           
            Assert.True(value.Amount > 0);
        }
    }
}